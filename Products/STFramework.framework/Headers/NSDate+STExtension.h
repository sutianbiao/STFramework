//
//  NSDate+STExtension.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (STExtension)

/**
 * 获取日、月、年、小时、分钟、秒
 */
- (NSUInteger)st_day;
- (NSUInteger)st_month;
- (NSUInteger)st_year;
- (NSUInteger)st_hour;
- (NSUInteger)st_minute;
- (NSUInteger)st_second;
+ (NSUInteger)st_day:(NSDate *)date;
+ (NSUInteger)st_month:(NSDate *)date;
+ (NSUInteger)st_year:(NSDate *)date;
+ (NSUInteger)st_hour:(NSDate *)date;
+ (NSUInteger)st_minute:(NSDate *)date;
+ (NSUInteger)st_second:(NSDate *)date;

/**
 * 获取一年中的总天数
 */
- (NSUInteger)st_daysInYear;
+ (NSUInteger)st_daysInYear:(NSDate *)date;

/**
 * 判断是否是润年
 * @return YES表示润年，NO表示平年
 */
- (BOOL)st_isLeapYear;
+ (BOOL)st_isLeapYear:(NSDate *)date;

/**
 * 获取该日期是该年的第几周
 */
- (NSUInteger)st_weekOfYear;
+ (NSUInteger)st_weekOfYear:(NSDate *)date;

/**
 * 获取格式化为YYYY-MM-dd格式的日期字符串
 */
- (NSString *)st_formatYMD;
+ (NSString *)st_formatYMD:(NSDate *)date;

/**
 * 返回当前月一共有几周(可能为4,5,6)
 */
- (NSUInteger)st_weeksOfMonth;
+ (NSUInteger)st_weeksOfMonth:(NSDate *)date;

/**
 * 获取该月的第一天的日期
 */
- (NSDate *)st_begindayOfMonth;
+ (NSDate *)st_begindayOfMonth:(NSDate *)date;

/**
 * 获取该月的最后一天的日期
 */
- (NSDate *)st_lastdayOfMonth;
+ (NSDate *)st_lastdayOfMonth:(NSDate *)date;

/**
 * 返回day天后的日期(若day为负数,则为|day|天前的日期)
 */
- (NSDate *)st_dateAfterDay:(NSUInteger)day;
+ (NSDate *)st_dateAfterDate:(NSDate *)date day:(NSInteger)day;

/**
 * 返回day天后的日期(若day为负数,则为|day|天前的日期)
 */
- (NSDate *)st_dateAfterMonth:(NSUInteger)month;
+ (NSDate *)st_dateAfterDate:(NSDate *)date month:(NSInteger)month;

/**
 * 返回numYears年后的日期
 */
- (NSDate *)st_offsetYears:(int)numYears;
+ (NSDate *)st_offsetYears:(int)numYears fromDate:(NSDate *)fromDate;

/**
 * 返回numMonths月后的日期
 */
- (NSDate *)st_offsetMonths:(int)numMonths;
+ (NSDate *)st_offsetMonths:(int)numMonths fromDate:(NSDate *)fromDate;

/**
 * 返回numDays天后的日期
 */
- (NSDate *)st_offsetDays:(int)numDays;
+ (NSDate *)st_offsetDays:(int)numDays fromDate:(NSDate *)fromDate;

/**
 * 返回numHours小时后的日期
 */
- (NSDate *)st_offsetHours:(int)hours;
+ (NSDate *)st_offsetHours:(int)numHours fromDate:(NSDate *)fromDate;

/**
 * 距离该日期前几天
 */
- (NSUInteger)st_daysAgo;
+ (NSUInteger)st_daysAgo:(NSDate *)date;

/**
 *  获取星期几
 *
 *  @return Return weekday number
 *  [1 - Sunday]
 *  [2 - Monday]
 *  [3 - Tuerday]
 *  [4 - Wednesday]
 *  [5 - Thursday]
 *  [6 - Friday]
 *  [7 - Saturday]
 */
- (NSInteger)st_weekday;
+ (NSInteger)st_weekday:(NSDate *)date;

/**
 *  获取星期几(名称)
 *
 *  @return Return weekday as a localized string
 *  [1 - Sunday]
 *  [2 - Monday]
 *  [3 - Tuerday]
 *  [4 - Wednesday]
 *  [5 - Thursday]
 *  [6 - Friday]
 *  [7 - Saturday]
 */
- (NSString *)st_dayFromWeekday;
+ (NSString *)st_dayFromWeekday:(NSDate *)date;

/**
 *  日期是否相等
 *
 *  @param anotherDate The another date to compare as NSDate
 *  @return Return YES if is same day, NO if not
 */
- (BOOL)st_isSameDay:(NSDate *)anotherDate;

/**
 *  是否是今天
 *
 *  @return Return if self is today
 */
- (BOOL)st_isToday;

/**
 *  Add days to self
 *
 *  @param days The number of days to add
 *  @return Return self by adding the gived days number
 */
- (NSDate *)st_dateByAddingDays:(NSUInteger)days;

/**
 *  Get the month as a localized string from the given month number
 *
 *  @param month The month to be converted in string
 *  [1 - January]
 *  [2 - February]
 *  [3 - March]
 *  [4 - April]
 *  [5 - May]
 *  [6 - June]
 *  [7 - July]
 *  [8 - August]
 *  [9 - September]
 *  [10 - October]
 *  [11 - November]
 *  [12 - December]
 *
 *  @return Return the given month as a localized string
 */
+ (NSString *)st_monthWithMonthNumber:(NSInteger)month;

/**
 * 根据日期返回字符串
 */
+ (NSString *)st_stringWithDate:(NSDate *)date format:(NSString *)format;
- (NSString *)st_stringWithFormat:(NSString *)format;
+ (NSDate *)st_dateWithString:(NSString *)string format:(NSString *)format;

/**
 * 获取指定月份的天数
 */
- (NSUInteger)st_daysInMonth:(NSUInteger)month;
+ (NSUInteger)st_daysInMonth:(NSDate *)date month:(NSUInteger)month;

/**
 * 获取当前月份的天数
 */
- (NSUInteger)st_daysInMonth;
+ (NSUInteger)st_daysInMonth:(NSDate *)date;

/**
 * 返回x分钟前/x小时前/昨天/x天前/x个月前/x年前
 */
- (NSString *)st_timeInfo;
+ (NSString *)st_timeInfoWithDate:(NSDate *)date;
+ (NSString *)st_timeInfoWithDateString:(NSString *)dateString;

/**
 * 分别获取yyyy-MM-dd/HH:mm:ss/yyyy-MM-dd HH:mm:ss格式的字符串
 */
- (NSString *)st_ymdFormat;
- (NSString *)st_hmsFormat;
- (NSString *)st_ymdHmsFormat;
+ (NSString *)st_ymdFormat;
+ (NSString *)st_hmsFormat;
+ (NSString *)st_ymdHmsFormat;

@end

NS_ASSUME_NONNULL_END
