//
//  NSObject+STKVOBlocks.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

// This class is based on NSObject+KVOBlocks (MIT licensed) by Stephan Leroux.
// See here: https://github.com/sleroux/KVO-Blocks


/*
 UIViewController *observer = self;
 if (self.observersOn) {
 // This is where the magic happens
 [self.user st_addObserver:observer forKeyPath:@"email" options:0 context:nil withBlock:^(NSDictionary *change, void *context) {
 NSLog(@"Changed email");
 }];
 
 [self.user st_addObserver:observer forKeyPath:@"username" options:0 context:nil withBlock:^(NSDictionary *change, void *context) {
 NSLog(@"Changed username");
 }];
 } else {
 [self.user st_removeBlockObserver:observer forKeyPath:@"username"];
 [self.user st_removeBlockObserver:observer forKeyPath:@"email"];
 }
 */

#import <Foundation/Foundation.h>
typedef void (^JKKVOBlock)(NSDictionary *change, void *context);

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (STKVOBlocks)

/**
 添加观察者与监听属性
 
 @param observer 观察者,一般为其他对象(谁想监听)
 @param keyPath 监听的属性
 @param options 监听模式
 @param context context
 @param block  监听回调
 */
- (void)st_addObserver:(NSObject *)observer
            forKeyPath:(NSString *)keyPath
               options:(NSKeyValueObservingOptions)options
               context:(void *)context
             withBlock:(JKKVOBlock)block;
/**
 移除观察者对属性的监听
 
 @param observer 观察者,一般为其他对象(谁想监听)
 @param keyPath 监听的属性
 */
-(void)st_removeBlockObserver:(NSObject *)observer
                   forKeyPath:(NSString *)keyPath;

/**
 对象本身作为观察者
 
 @param keyPath 监听的属性
 @param options 监听模式
 @param context context
 @param block 监听回调
 */
-(void)st_addObserverForKeyPath:(NSString *)keyPath
                        options:(NSKeyValueObservingOptions)options
                        context:(void *)context
                      withBlock:(JKKVOBlock)block;

/**
 移除观察者对属性的监听
 
 @param keyPath 监听的属性
 */
-(void)st_removeBlockObserverForKeyPath:(NSString *)keyPath;

@end

NS_ASSUME_NONNULL_END
