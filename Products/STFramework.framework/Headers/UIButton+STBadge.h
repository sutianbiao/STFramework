//
//  UIButton+STBadge.h
//  STFramework
//  脚标
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (STBadge)

@property (strong, nonatomic) UILabel *st_badge;

// Badge value to be display
@property (nonatomic) NSString *st_badgeValue;
// Badge background color
@property (nonatomic) UIColor *st_badgeBGColor;
// Badge text color
@property (nonatomic) UIColor *st_badgeTextColor;
// Badge font
@property (nonatomic) UIFont *st_badgeFont;
// Padding value for the badge
@property (nonatomic) CGFloat st_badgePadding;
// Minimum size badge to small
@property (nonatomic) CGFloat st_badgeMinSize;
// Values for offseting the badge over the BarButtonItem you picked
@property (nonatomic) CGFloat st_badgeOriginX;
@property (nonatomic) CGFloat st_badgeOriginY;
// In case of numbers, remove the badge when reaching zero
@property BOOL st_shouldHideBadgeAtZero;
// Badge has a bounce animation when value changes
@property BOOL st_shouldAnimateBadge;

@end

NS_ASSUME_NONNULL_END
