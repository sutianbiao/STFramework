//
//  UIControl+STActionBlocks.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^UIControlSTActionBlock)(id weakSender);

@interface UIControlSTActionBlockWrapper : NSObject
@property (nonatomic, copy) UIControlSTActionBlock st_actionBlock;
@property (nonatomic, assign) UIControlEvents st_controlEvents;
- (void)st_invokeBlock:(id)sender;
@end

@interface UIControl (STActionBlocks)

- (void)st_handleControlEvents:(UIControlEvents)controlEvents withBlock:(UIControlSTActionBlock)actionBlock;
- (void)st_removeActionBlocksForControlEvents:(UIControlEvents)controlEvents;

@end

NS_ASSUME_NONNULL_END
