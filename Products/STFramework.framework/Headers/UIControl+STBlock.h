//
//  UIControl+STBlock.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIControl (STBlock)

- (void)st_touchDown:(void (^)(void))eventBlock;
- (void)st_touchDownRepeat:(void (^)(void))eventBlock;
- (void)st_touchDragInside:(void (^)(void))eventBlock;
- (void)st_touchDragOutside:(void (^)(void))eventBlock;
- (void)st_touchDragEnter:(void (^)(void))eventBlock;
- (void)st_touchDragExit:(void (^)(void))eventBlock;
- (void)st_touchUpInside:(void (^)(void))eventBlock;
- (void)st_touchUpOutside:(void (^)(void))eventBlock;
- (void)st_touchCancel:(void (^)(void))eventBlock;
- (void)st_valueChanged:(void (^)(void))eventBlock;
- (void)st_editingDidBegin:(void (^)(void))eventBlock;
- (void)st_editingChanged:(void (^)(void))eventBlock;
- (void)st_editingDidEnd:(void (^)(void))eventBlock;
- (void)st_editingDidEndOnExit:(void (^)(void))eventBlock;

@end

NS_ASSUME_NONNULL_END
