//
//  UILabel+STAdjustableLabel.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (STAdjustableLabel)

// General method. If minSize is set to CGSizeZero then
// it is ignored
// =====================================================
- (void)st_adjustLabelToMaximumSize:(CGSize)maxSize
                        minimumSize:(CGSize)minSize
                    minimumFontSize:(int)minFontSize;

// Adjust label using only the maximum size and the
// font size as constraints
// =====================================================
- (void)st_adjustLabelToMaximumSize:(CGSize)maxSize
                    minimumFontSize:(int)minFontSize;

// Adjust the size of the label using only the font
// size as a constraint (the maximum size will be
// calculated automatically based on the screen size)
// =====================================================
- (void)st_adjustLabelSizeWithMinimumFontSize:(int)minFontSize;

// Adjust label without any constraints (the maximum
// size will be calculated automatically based on the
// screen size)
// =====================================================
- (void)st_adjustLabel;

@end

NS_ASSUME_NONNULL_END
