//
//  UIView+STBlur.h
//  STFramework
//  模糊
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    UIViewSTBlurLightStyle,
    UIViewSTBlurDarkStyle
} UIViewSTBlurStyle;

@interface UIView (STBlur)

/* The UIToolbar that has been put on the current view, use it to do your bidding */
@property (strong,nonatomic,readonly) UIView* blurBackground;

/* tint color of the blurred view */
@property (strong,nonatomic) UIColor* blurTintColor;

/* intensity of blurTintColor applied on the blur 0.0-1.0, default 0.6f */
@property (assign,nonatomic) CGFloat blurTintColorIntensity;

/* returns if blurring is enabled */
@property (readonly,nonatomic) BOOL isSTBlurred;

/* Style of Toolbar, remapped to UIViewSTBlurStyle typedef above */
@property (assign,nonatomic) UIViewSTBlurStyle blurStyle;

/* method to enable STBlur on current UIView */
-(void)enableSTBlur:(BOOL) enable;

@end
