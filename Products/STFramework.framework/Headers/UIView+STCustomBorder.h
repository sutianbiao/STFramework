//
//  UIView+STCustomBorder.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, STExcludePoint) {
    STExcludeStartPoint = 1 << 0,
    STExcludeEndPoint = 1 << 1,
    STExcludeAllPoint = ~0UL
};

@interface UIView (STCustomBorder)

- (void)st_addTopBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth;
- (void)st_addLeftBorderWithColor: (UIColor *) color width:(CGFloat) borderWidth;
- (void)st_addBottomBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth;
- (void)st_addRightBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth;

- (void)st_removeTopBorder;
- (void)st_removeLeftBorder;
- (void)st_removeBottomBorder;
- (void)st_removeRightBorder;


- (void)st_addTopBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth excludePoint:(CGFloat)point edgeType:(STExcludePoint)edge;
- (void)st_addLeftBorderWithColor: (UIColor *) color width:(CGFloat) borderWidth excludePoint:(CGFloat)point edgeType:(STExcludePoint)edge;
- (void)st_addBottomBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth excludePoint:(CGFloat)point edgeType:(STExcludePoint)edge;
- (void)st_addRightBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth excludePoint:(CGFloat)point edgeType:(STExcludePoint)edge;

@end

NS_ASSUME_NONNULL_END
