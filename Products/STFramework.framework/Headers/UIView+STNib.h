//
//  UIView+STSTNib.h
//  STFramework
//  Nib
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (STNib)
+ (UINib *)loadSTNib;
/**
 *  快速根据xib创建View
 */
+ (instancetype )loadSTNibView;

+ (UINib *)loadSTNibNamed:(NSString*)nibName;
+ (UINib *)loadSTNibNamed:(NSString*)nibName bundle:(NSBundle *)bundle;

+ (instancetype)loadInstanceFromSTNib;
+ (instancetype)loadInstanceFromSTNibWithName:(NSString *)nibName;
+ (instancetype)loadInstanceFromSTNibWithName:(NSString *)nibName owner:(id)owner;
+ (instancetype)loadInstanceFromSTNibWithName:(NSString *)nibName owner:(id)owner bundle:(NSBundle *)bundle;

@end

NS_ASSUME_NONNULL_END
