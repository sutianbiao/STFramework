//
//  CALayer+STPauseAimate.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface CALayer (STPauseAimate)

/** 暂停动画*/
- (void)pauseAnimate;

/** 恢复动画*/
- (void)resumeAnimate;

@end

NS_ASSUME_NONNULL_END
