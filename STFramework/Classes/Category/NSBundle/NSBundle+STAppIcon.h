//
//  NSBundle+STAppIcon.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSBundle (STAppIcon)

- (NSString*)st_appIconPath ;
- (UIImage*)st_appIcon ;

@end

NS_ASSUME_NONNULL_END
