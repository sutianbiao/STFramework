//
//  NSBundle+STAppIcon.m
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "NSBundle+STAppIcon.h"

@implementation NSBundle (STAppIcon)

- (NSString*)st_appIconPath {
    NSString* iconFilename = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIconFile"] ;
    NSString* iconBasename = [iconFilename stringByDeletingPathExtension] ;
    NSString* iconExtension = [iconFilename pathExtension] ;
    return [[NSBundle mainBundle] pathForResource:iconBasename
                                           ofType:iconExtension] ;
}

- (UIImage*)st_appIcon {
    UIImage*appIcon = [[UIImage alloc] initWithContentsOfFile:[self st_appIconPath]] ;
    return appIcon;
}

@end
