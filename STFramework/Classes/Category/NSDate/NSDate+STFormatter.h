//
//  NSDate+STFormatter.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (STFormatter)

+(NSDateFormatter *)st_formatter;
+(NSDateFormatter *)st_formatterWithoutTime;
+(NSDateFormatter *)st_formatterWithoutDate;

-(NSString *)st_formatWithUTCTimeZone;
-(NSString *)st_formatWithLocalTimeZone;
-(NSString *)st_formatWithTimeZoneOffset:(NSTimeInterval)offset;
-(NSString *)st_formatWithTimeZone:(NSTimeZone *)timezone;

-(NSString *)st_formatWithUTCTimeZoneWithoutTime;
-(NSString *)st_formatWithLocalTimeZoneWithoutTime;
-(NSString *)st_formatWithTimeZoneOffsetWithoutTime:(NSTimeInterval)offset;
-(NSString *)st_formatWithTimeZoneWithoutTime:(NSTimeZone *)timezone;

-(NSString *)st_formatWithUTCWithoutDate;
-(NSString *)st_formatWithLocalTimeWithoutDate;
-(NSString *)st_formatWithTimeZoneOffsetWithoutDate:(NSTimeInterval)offset;
-(NSString *)st_formatTimeWithTimeZone:(NSTimeZone *)timezone;


+ (NSString *)st_currentDateStringWithFormat:(NSString *)format;
+ (NSDate *)st_dateWithSecondsFromNow:(NSInteger)seconds;
+ (NSDate *)st_dateWithYear:(NSInteger)year month:(NSUInteger)month day:(NSUInteger)day;
- (NSString *)st_dateWithFormat:(NSString *)format;

@end

NS_ASSUME_NONNULL_END
