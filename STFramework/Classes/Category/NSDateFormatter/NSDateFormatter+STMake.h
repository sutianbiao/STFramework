//
//  NSDateFormatter+STMake.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDateFormatter (STMake)

+(NSDateFormatter *)st_dateFormatterWithFormat:(NSString *)format;
+(NSDateFormatter *)st_dateFormatterWithFormat:(NSString *)format timeZone:(NSTimeZone *)timeZone;
+(NSDateFormatter *)st_dateFormatterWithFormat:(NSString *)format timeZone:(NSTimeZone *)timeZone locale:(NSLocale *)locale;
+(NSDateFormatter *)st_dateFormatterWithDateStyle:(NSDateFormatterStyle)style;
+(NSDateFormatter *)st_dateFormatterWithDateStyle:(NSDateFormatterStyle)style timeZone:(NSTimeZone *)timeZone;
+(NSDateFormatter *)st_dateFormatterWithTimeStyle:(NSDateFormatterStyle)style;
+(NSDateFormatter *)st_dateFormatterWithTimeStyle:(NSDateFormatterStyle)style timeZone:(NSTimeZone *)timeZone;

@end

NS_ASSUME_NONNULL_END
