//
//  NSNumber+STAppInfo.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSNumber (STAppInfo)

-(NSString *)st_version;
-(NSInteger)st_build;
-(NSString *)st_identifier;
-(NSString *)st_currentLanguage;
-(NSString *)st_deviceModel;

@end

NS_ASSUME_NONNULL_END
