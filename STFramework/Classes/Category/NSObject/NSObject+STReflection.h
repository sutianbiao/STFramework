//
//  NSObject+STReflection.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (STReflection)

//类名
- (NSString *)st_className;
+ (NSString *)st_className;
//父类名称
- (NSString *)st_superClassName;
+ (NSString *)st_superClassName;

//实例属性字典
-(NSDictionary *)st_propertyDictionary;

//属性名称列表
- (NSArray*)st_propertyKeys;
+ (NSArray *)st_propertyKeys;

//属性详细信息列表
- (NSArray *)st_propertiesInfo;
+ (NSArray *)st_propertiesInfo;

//格式化后的属性列表
+ (NSArray *)st_propertiesWithCodeFormat;

//方法列表
-(NSArray*)st_methodList;
+(NSArray*)st_methodList;

-(NSArray*)st_methodListInfo;

//创建并返回一个指向所有已注册类的指针列表
+ (NSArray *)st_registedClassList;
//实例变量
+ (NSArray *)st_instanceVariable;

//协议列表
-(NSDictionary *)st_protocolList;
+ (NSDictionary *)st_protocolList;

- (BOOL)st_hasPropertyForKey:(NSString*)key;
- (BOOL)st_hasIvarForKey:(NSString*)key;

@end

NS_ASSUME_NONNULL_END
