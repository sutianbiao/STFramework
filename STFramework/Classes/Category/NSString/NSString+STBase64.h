//
//  NSString+STBase64.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (STBase64)

+ (NSString *)st_stringWithBase64EncodedString:(NSString *)string;
- (NSString *)st_base64EncodedStringWithWrapWidth:(NSUInteger)wrapWidth;
- (NSString *)st_base64EncodedString;
- (NSString *)st_base64DecodedString;
- (NSData *)st_base64DecodedData;

@end

NS_ASSUME_NONNULL_END
