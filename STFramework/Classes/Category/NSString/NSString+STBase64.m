//
//  NSString+STBase64.m
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "NSString+STBase64.h"
#import "NSData+STBase64.h"

@implementation NSString (STBase64)

+ (NSString *)st_stringWithBase64EncodedString:(NSString *)string
{
    NSData *data = [NSData st_dataWithBase64EncodedString:string];
    if (data)
    {
        return [[self alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    return nil;
}
- (NSString *)st_base64EncodedStringWithWrapWidth:(NSUInteger)wrapWidth
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    return [data st_base64EncodedStringWithWrapWidth:wrapWidth];
}
- (NSString *)st_base64EncodedString
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    return [data st_base64EncodedString];
}
- (NSString *)st_base64DecodedString
{
    return [NSString st_stringWithBase64EncodedString:self];
}
- (NSData *)st_base64DecodedData
{
    return [NSData st_dataWithBase64EncodedString:self];
}

@end
