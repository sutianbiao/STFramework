//
//  NSString+STEncrypt.m
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "NSString+STEncrypt.h"
#import "NSData+STEncrypt.h"
#import "NSData+STBase64.h"

@implementation NSString (STEncrypt)

-(NSString*)st_encryptedWithAESUsingKey:(NSString*)key andIV:(NSData*)iv {
    NSData *encrypted = [[self dataUsingEncoding:NSUTF8StringEncoding] st_encryptedWithAESUsingKey:key andIV:iv];
    NSString *encryptedString = [encrypted st_base64EncodedString];
    
    return encryptedString;
}

- (NSString*)st_decryptedWithAESUsingKey:(NSString*)key andIV:(NSData*)iv {
    NSData *decrypted = [[NSData st_dataWithBase64EncodedString:self] st_decryptedWithAESUsingKey:key andIV:iv];
    NSString *decryptedString = [[NSString alloc] initWithData:decrypted encoding:NSUTF8StringEncoding];
    
    return decryptedString;
}

- (NSString*)st_encryptedWithDESUsingKey:(NSString*)key andIV:(NSData*)iv {
    NSData *encrypted = [[self dataUsingEncoding:NSUTF8StringEncoding] st_encryptedWithDESUsingKey:key andIV:iv];
    NSString *encryptedString = [encrypted st_base64EncodedString];
    
    return encryptedString;
}

- (NSString*)st_decryptedWithDESUsingKey:(NSString*)key andIV:(NSData*)iv {
    NSData *decrypted = [[NSData st_dataWithBase64EncodedString:self] st_decryptedWithDESUsingKey:key andIV:iv];
    NSString *decryptedString = [[NSString alloc] initWithData:decrypted encoding:NSUTF8StringEncoding];
    
    return decryptedString;
}

- (NSString*)st_encryptedWith3DESUsingKey:(NSString*)key andIV:(NSData*)iv {
    NSData *encrypted = [[self dataUsingEncoding:NSUTF8StringEncoding] st_encryptedWith3DESUsingKey:key andIV:iv];
    NSString *encryptedString = [encrypted st_base64EncodedString];
    
    return encryptedString;
}

- (NSString*)st_decryptedWith3DESUsingKey:(NSString*)key andIV:(NSData*)iv {
    NSData *decrypted = [[NSData st_dataWithBase64EncodedString:self] st_decryptedWith3DESUsingKey:key andIV:iv];
    NSString *decryptedString = [[NSString alloc] initWithData:decrypted encoding:NSUTF8StringEncoding];
    
    return decryptedString;
}

@end
