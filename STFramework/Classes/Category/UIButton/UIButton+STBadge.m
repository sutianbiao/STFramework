//
//  UIButton+STBadge.m
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//
#import <objc/runtime.h>
#import "UIButton+STBadge.h"

NSString const *st_UIButton_badgeKey = @"st_UIButton_badgeKey";

NSString const *st_UIButton_badgeBGColorKey = @"st_UIButton_badgeBGColorKey";
NSString const *st_UIButton_badgeTextColorKey = @"st_UIButton_badgeTextColorKey";
NSString const *st_UIButton_badgeFontKey = @"st_UIButton_badgeFontKey";
NSString const *st_UIButton_badgePaddingKey = @"st_UIButton_badgePaddingKey";
NSString const *st_UIButton_badgeMinSizeKey = @"st_UIButton_badgeMinSizeKey";
NSString const *st_UIButton_badgeOriginXKey = @"st_UIButton_badgeOriginXKey";
NSString const *st_UIButton_badgeOriginYKey = @"st_UIButton_badgeOriginYKey";
NSString const *st_UIButton_shouldHideBadgeAtZeroKey = @"st_UIButton_shouldHideBadgeAtZeroKey";
NSString const *st_UIButton_shouldAnimateBadgeKey = @"st_UIButton_shouldAnimateBadgeKey";
NSString const *st_UIButton_badgeValueKey = @"st_UIButton_badgeValueKey";

@implementation UIButton (STBadge)

@dynamic st_badgeValue, st_badgeBGColor, st_badgeTextColor, st_badgeFont;
@dynamic st_badgePadding, st_badgeMinSize, st_badgeOriginX, st_badgeOriginY;
@dynamic st_shouldHideBadgeAtZero, st_shouldAnimateBadge;

- (void)st_badgeInit
{
    // Default design initialization
    self.st_badgeBGColor   = [UIColor redColor];
    self.st_badgeTextColor = [UIColor whiteColor];
    self.st_badgeFont      = [UIFont systemFontOfSize:12.0];
    self.st_badgePadding   = 6;
    self.st_badgeMinSize   = 8;
    self.st_badgeOriginX   = self.frame.size.width - self.st_badge.frame.size.width/2;
    self.st_badgeOriginY   = -4;
    self.st_shouldHideBadgeAtZero = YES;
    self.st_shouldAnimateBadge = YES;
    // Avoids badge to be clipped when animating its scale
    self.clipsToBounds = NO;
}

#pragma mark - Utility methods

// Handle badge display when its properties have been changed (color, font, ...)
- (void)st_refreshBadge
{
    // Change new attributes
    self.st_badge.textColor        = self.st_badgeTextColor;
    self.st_badge.backgroundColor  = self.st_badgeBGColor;
    self.st_badge.font             = self.st_badgeFont;
}

- (CGSize) st_badgeExpectedSize
{
    // When the value changes the badge could need to get bigger
    // Calculate expected size to fit new value
    // Use an intermediate label to get expected size thanks to sizeToFit
    // We don't call sizeToFit on the true label to avoid bad display
    UILabel *frameLabel = [self st_duplicateLabel:self.st_badge];
    [frameLabel sizeToFit];
    
    CGSize expectedLabelSize = frameLabel.frame.size;
    return expectedLabelSize;
}

- (void)st_updateBadgeFrame
{
    
    CGSize expectedLabelSize = [self st_badgeExpectedSize];
    
    // Make sure that for small value, the badge will be big enough
    CGFloat minHeight = expectedLabelSize.height;
    
    // Using a const we make sure the badge respect the minimum size
    minHeight = (minHeight < self.st_badgeMinSize) ? self.st_badgeMinSize : expectedLabelSize.height;
    CGFloat minWidth = expectedLabelSize.width;
    CGFloat padding = self.st_badgePadding;
    
    // Using const we make sure the badge doesn't get too smal
    minWidth = (minWidth < minHeight) ? minHeight : expectedLabelSize.width;
    self.st_badge.frame = CGRectMake(self.st_badgeOriginX, self.st_badgeOriginY, minWidth + padding, minHeight + padding);
    self.st_badge.layer.cornerRadius = (minHeight + padding) / 2;
    self.st_badge.layer.masksToBounds = YES;
}

// Handle the badge changing value
- (void)st_updateBadgeValueAnimated:(BOOL)animated
{
    // Bounce animation on badge if value changed and if animation authorized
    if (animated && self.st_shouldAnimateBadge && ![self.st_badge.text isEqualToString:self.st_badgeValue]) {
        CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [animation setFromValue:[NSNumber numberWithFloat:1.5]];
        [animation setToValue:[NSNumber numberWithFloat:1]];
        [animation setDuration:0.2];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithControlPoints:.4f :1.3f :1.f :1.f]];
        [self.st_badge.layer addAnimation:animation forKey:@"bounceAnimation"];
    }
    
    // Set the new value
    self.st_badge.text = self.st_badgeValue;
    
    // Animate the size modification if needed
    NSTimeInterval duration = animated ? 0.2 : 0;
    [UIView animateWithDuration:duration animations:^{
        [self st_updateBadgeFrame];
    }];
}

- (UILabel *)st_duplicateLabel:(UILabel *)labelToCopy
{
    UILabel *duplicateLabel = [[UILabel alloc] initWithFrame:labelToCopy.frame];
    duplicateLabel.text = labelToCopy.text;
    duplicateLabel.font = labelToCopy.font;
    
    return duplicateLabel;
}

- (void)st_removeBadge
{
    // Animate badge removal
    [UIView animateWithDuration:0.2 animations:^{
        self.st_badge.transform = CGAffineTransformMakeScale(0, 0);
    } completion:^(BOOL finished) {
        [self.st_badge removeFromSuperview];
        self.st_badge = nil;
    }];
}

#pragma mark - getters/setters
-(UILabel*)st_badge {
    return objc_getAssociatedObject(self, &st_UIButton_badgeKey);
}
-(void)setSt_badge:(UILabel *)badgeLabel
{
    objc_setAssociatedObject(self, &st_UIButton_badgeKey, badgeLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

// Badge value to be display
-(NSString *)st_badgeValue {
    return objc_getAssociatedObject(self, &st_UIButton_badgeValueKey);
}
-(void) setSt_badgeValue:(NSString *)badgeValue
{
    objc_setAssociatedObject(self, &st_UIButton_badgeValueKey, badgeValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    // When changing the badge value check if we need to remove the badge
    if (!badgeValue || [badgeValue isEqualToString:@""] || ([badgeValue isEqualToString:@"0"] && self.st_shouldHideBadgeAtZero)) {
        [self st_removeBadge];
    } else if (!self.st_badge) {
        // Create a new badge because not existing
        self.st_badge                      = [[UILabel alloc] initWithFrame:CGRectMake(self.st_badgeOriginX, self.st_badgeOriginY, 20, 20)];
        self.st_badge.textColor            = self.st_badgeTextColor;
        self.st_badge.backgroundColor      = self.st_badgeBGColor;
        self.st_badge.font                 = self.st_badgeFont;
        self.st_badge.textAlignment        = NSTextAlignmentCenter;
        [self st_badgeInit];
        [self addSubview:self.st_badge];
        [self st_updateBadgeValueAnimated:NO];
    } else {
        [self st_updateBadgeValueAnimated:YES];
    }
}

// Badge background color
-(UIColor *)st_badgeBGColor {
    return objc_getAssociatedObject(self, &st_UIButton_badgeBGColorKey);
}
-(void)setSt_badgeBGColor:(UIColor *)badgeBGColor
{
    objc_setAssociatedObject(self, &st_UIButton_badgeBGColorKey, badgeBGColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.st_badge) {
        [self st_refreshBadge];
    }
}

// Badge text color
-(UIColor *)st_badgeTextColor {
    return objc_getAssociatedObject(self, &st_UIButton_badgeTextColorKey);
}
-(void)setSt_badgeTextColor:(UIColor *)badgeTextColor
{
    objc_setAssociatedObject(self, &st_UIButton_badgeTextColorKey, badgeTextColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.st_badge) {
        [self st_refreshBadge];
    }
}

// Badge font
-(UIFont *)st_badgeFont {
    return objc_getAssociatedObject(self, &st_UIButton_badgeFontKey);
}
-(void)setSt_badgeFont:(UIFont *)badgeFont
{
    objc_setAssociatedObject(self, &st_UIButton_badgeFontKey, badgeFont, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.st_badge) {
        [self st_refreshBadge];
    }
}

// Padding value for the badge
-(CGFloat) st_badgePadding {
    NSNumber *number = objc_getAssociatedObject(self, &st_UIButton_badgePaddingKey);
    return number.floatValue;
}
-(void) setSt_badgePadding:(CGFloat)badgePadding
{
    NSNumber *number = [NSNumber numberWithDouble:badgePadding];
    objc_setAssociatedObject(self, &st_UIButton_badgePaddingKey, number, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.st_badge) {
        [self st_updateBadgeFrame];
    }
}

// Minimum size badge to small
-(CGFloat) st_badgeMinSize {
    NSNumber *number = objc_getAssociatedObject(self, &st_UIButton_badgeMinSizeKey);
    return number.floatValue;
}
-(void) setSt_badgeMinSize:(CGFloat)badgeMinSize
{
    NSNumber *number = [NSNumber numberWithDouble:badgeMinSize];
    objc_setAssociatedObject(self, &st_UIButton_badgeMinSizeKey, number, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.st_badge) {
        [self st_updateBadgeFrame];
    }
}

// Values for offseting the badge over the BarButtonItem you picked
-(CGFloat) st_badgeOriginX {
    NSNumber *number = objc_getAssociatedObject(self, &st_UIButton_badgeOriginXKey);
    return number.floatValue;
}
-(void) setSt_badgeOriginX:(CGFloat)badgeOriginX
{
    NSNumber *number = [NSNumber numberWithDouble:badgeOriginX];
    objc_setAssociatedObject(self, &st_UIButton_badgeOriginXKey, number, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.st_badge) {
        [self st_updateBadgeFrame];
    }
}

-(CGFloat) st_badgeOriginY {
    NSNumber *number = objc_getAssociatedObject(self, &st_UIButton_badgeOriginYKey);
    return number.floatValue;
}
-(void) setSt_badgeOriginY:(CGFloat)badgeOriginY
{
    NSNumber *number = [NSNumber numberWithDouble:badgeOriginY];
    objc_setAssociatedObject(self, &st_UIButton_badgeOriginYKey, number, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.st_badge) {
        [self st_updateBadgeFrame];
    }
}

// In case of numbers, remove the badge when reaching zero
-(BOOL) st_shouldHideBadgeAtZero {
    NSNumber *number = objc_getAssociatedObject(self, &st_UIButton_shouldHideBadgeAtZeroKey);
    return number.boolValue;
}
- (void)setSt_shouldHideBadgeAtZero:(BOOL)shouldHideBadgeAtZero
{
    NSNumber *number = [NSNumber numberWithBool:shouldHideBadgeAtZero];
    objc_setAssociatedObject(self, &st_UIButton_shouldHideBadgeAtZeroKey, number, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

// Badge has a bounce animation when value changes
-(BOOL) st_shouldAnimateBadge {
    NSNumber *number = objc_getAssociatedObject(self, &st_UIButton_shouldAnimateBadgeKey);
    return number.boolValue;
}
- (void)setSt_shouldAnimateBadge:(BOOL)shouldAnimateBadge
{
    NSNumber *number = [NSNumber numberWithBool:shouldAnimateBadge];
    objc_setAssociatedObject(self, &st_UIButton_shouldAnimateBadgeKey, number, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
