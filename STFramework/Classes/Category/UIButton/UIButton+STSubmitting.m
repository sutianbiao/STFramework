//
//  UIButton+STSubmitting.m
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "UIButton+STSubmitting.h"
#import  <objc/runtime.h>

@interface UIButton ()

@property(nonatomic, strong) UIView *st_modalView;
@property(nonatomic, strong) UIActivityIndicatorView *st_spinnerView;
@property(nonatomic, strong) UILabel *st_spinnerTitleLabel;

@end

@implementation UIButton (STSubmitting)

- (void)st_beginSubmitting:(NSString *)title {
    [self st_endSubmitting];
    
    self.st_submitting = @YES;
    self.hidden = YES;
    
    self.st_modalView = [[UIView alloc] initWithFrame:self.frame];
    self.st_modalView.backgroundColor =
    [self.backgroundColor colorWithAlphaComponent:0.6];
    self.st_modalView.layer.cornerRadius = self.layer.cornerRadius;
    self.st_modalView.layer.borderWidth = self.layer.borderWidth;
    self.st_modalView.layer.borderColor = self.layer.borderColor;
    
    CGRect viewBounds = self.st_modalView.bounds;
    self.st_spinnerView = [[UIActivityIndicatorView alloc]
                           initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.st_spinnerView.tintColor = self.titleLabel.textColor;
    
    CGRect spinnerViewBounds = self.st_spinnerView.bounds;
    self.st_spinnerView.frame = CGRectMake(
                                           15, viewBounds.size.height / 2 - spinnerViewBounds.size.height / 2,
                                           spinnerViewBounds.size.width, spinnerViewBounds.size.height);
    self.st_spinnerTitleLabel = [[UILabel alloc] initWithFrame:viewBounds];
    self.st_spinnerTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.st_spinnerTitleLabel.text = title;
    self.st_spinnerTitleLabel.font = self.titleLabel.font;
    self.st_spinnerTitleLabel.textColor = self.titleLabel.textColor;
    [self.st_modalView addSubview:self.st_spinnerView];
    [self.st_modalView addSubview:self.st_spinnerTitleLabel];
    [self.superview addSubview:self.st_modalView];
    [self.st_spinnerView startAnimating];
}

- (void)st_endSubmitting {
    if (!self.isSTSubmitting.boolValue) {
        return;
    }
    
    self.st_submitting = @NO;
    self.hidden = NO;
    
    [self.st_modalView removeFromSuperview];
    self.st_modalView = nil;
    self.st_spinnerView = nil;
    self.st_spinnerTitleLabel = nil;
}

- (NSNumber *)isSTSubmitting {
    return objc_getAssociatedObject(self, @selector(setSt_submitting:));
}

- (void)setSt_submitting:(NSNumber *)submitting {
    objc_setAssociatedObject(self, @selector(setSt_submitting:), submitting, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}

- (UIActivityIndicatorView *)st_spinnerView {
    return objc_getAssociatedObject(self, @selector(setSt_spinnerView:));
}

- (void)setSt_spinnerView:(UIActivityIndicatorView *)spinnerView {
    objc_setAssociatedObject(self, @selector(setSt_spinnerView:), spinnerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)st_modalView {
    return objc_getAssociatedObject(self, @selector(setSt_modalView:));
    
}

- (void)setSt_modalView:(UIView *)modalView {
    objc_setAssociatedObject(self, @selector(setSt_modalView:), modalView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UILabel *)st_spinnerTitleLabel {
    return objc_getAssociatedObject(self, @selector(setSt_spinnerTitleLabel:));
}

- (void)setSt_spinnerTitleLabel:(UILabel *)spinnerTitleLabel {
    objc_setAssociatedObject(self, @selector(setSt_spinnerTitleLabel:), spinnerTitleLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
