//
//  UIColor+STExtension.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

#define rgb(r,g,b) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0f]
#define rgba(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define hexColor(colorV) [UIColor colorWithHexColorString:@#colorV]
#define hexColorAlpha(colorV,a) [UIColor colorWithHexColorString:@#colorV alpha:a];

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (STExtension)

/**
 *  十六进制颜色
 */
+ (UIColor *)colorWithHexColorString:(NSString *)hexColorString;

/**
 *  十六进制颜色:含alpha
 */
+ (UIColor *)colorWithHexColorString:(NSString *)hexColorString alpha:(float)alpha;

/**
 *  十六进制颜色
 */
+ (UIColor *)colorWithHex:(long)hexColor;

/**
 *  十六进制颜色:含alpha
 */
+ (UIColor *)colorWithHex:(long)hexColor alpha:(CGFloat)alpha;

/**
 *  rgb转颜色
 */
+ (UIColor *)colorWithR:(CGFloat)r g:(CGFloat)g b:(CGFloat)b a:(CGFloat)a;

/**
 *  UIColor转UIImage
 */
+ (UIImage *)createImageWithColor:(UIColor *)color;



@end

NS_ASSUME_NONNULL_END
