//
//  UIScrollView+STAddition.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, STScrollDirection) {
    STScrollDirectionUp,
    STScrollDirectionDown,
    STScrollDirectionLeft,
    STScrollDirectionRight,
    STScrollDirectionWTF
};

@interface UIScrollView (STAddition)

@property(nonatomic) CGFloat st_contentWidth;
@property(nonatomic) CGFloat st_contentHeight;
@property(nonatomic) CGFloat st_contentOffsetX;
@property(nonatomic) CGFloat st_contentOffsetY;

- (CGPoint)st_topContentOffset;
- (CGPoint)st_bottomContentOffset;
- (CGPoint)st_leftContentOffset;
- (CGPoint)st_rightContentOffset;

- (STScrollDirection)st_ScrollDirection;

- (BOOL)st_isScrolledToTop;
- (BOOL)st_isScrolledToBottom;
- (BOOL)st_isScrolledToLeft;
- (BOOL)st_isScrolledToRight;
- (void)st_scrollToTopAnimated:(BOOL)animated;
- (void)st_scrollToBottomAnimated:(BOOL)animated;
- (void)st_scrollToLeftAnimated:(BOOL)animated;
- (void)st_scrollToRightAnimated:(BOOL)animated;

- (NSUInteger)st_verticalPageIndex;
- (NSUInteger)st_horizontalPageIndex;

- (void)st_scrollToVerticalPageIndex:(NSUInteger)pageIndex animated:(BOOL)animated;
- (void)st_scrollToHorizontalPageIndex:(NSUInteger)pageIndex animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
