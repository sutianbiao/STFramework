//
//  UIScrollView+STAddition.m
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "UIScrollView+STAddition.h"

@implementation UIScrollView (STAddition)

//frame
- (CGFloat)st_contentWidth {
    return self.contentSize.width;
}
- (void)setSt_contentWidth:(CGFloat)width {
    self.contentSize = CGSizeMake(width, self.frame.size.height);
}
- (CGFloat)st_contentHeight {
    return self.contentSize.height;
}
- (void)setSt_contentHeight:(CGFloat)height {
    self.contentSize = CGSizeMake(self.frame.size.width, height);
}
- (CGFloat)st_contentOffsetX {
    return self.contentOffset.x;
}
- (void)setSt_contentOffsetX:(CGFloat)x {
    self.contentOffset = CGPointMake(x, self.contentOffset.y);
}
- (CGFloat)st_contentOffsetY {
    return self.contentOffset.y;
}
- (void)setSt_contentOffsetY:(CGFloat)y {
    self.contentOffset = CGPointMake(self.contentOffset.x, y);
}
//


- (CGPoint)st_topContentOffset
{
    return CGPointMake(0.0f, -self.contentInset.top);
}
- (CGPoint)st_bottomContentOffset
{
    return CGPointMake(0.0f, self.contentSize.height + self.contentInset.bottom - self.bounds.size.height);
}
- (CGPoint)st_leftContentOffset
{
    return CGPointMake(-self.contentInset.left, 0.0f);
}
- (CGPoint)st_rightContentOffset
{
    return CGPointMake(self.contentSize.width + self.contentInset.right - self.bounds.size.width, 0.0f);
}
- (STScrollDirection)st_ScrollDirection
{
    STScrollDirection direction;
    
    if ([self.panGestureRecognizer translationInView:self.superview].y > 0.0f)
    {
        direction = STScrollDirectionUp;
    }
    else if ([self.panGestureRecognizer translationInView:self.superview].y < 0.0f)
    {
        direction = STScrollDirectionDown;
    }
    else if ([self.panGestureRecognizer translationInView:self].x < 0.0f)
    {
        direction = STScrollDirectionLeft;
    }
    else if ([self.panGestureRecognizer translationInView:self].x > 0.0f)
    {
        direction = STScrollDirectionRight;
    }
    else
    {
        direction = STScrollDirectionWTF;
    }
    
    return direction;
}
- (BOOL)st_isScrolledToTop
{
    return self.contentOffset.y <= [self st_topContentOffset].y;
}
- (BOOL)st_isScrolledToBottom
{
    return self.contentOffset.y >= [self st_bottomContentOffset].y;
}
- (BOOL)st_isScrolledToLeft
{
    return self.contentOffset.x <= [self st_leftContentOffset].x;
}
- (BOOL)st_isScrolledToRight
{
    return self.contentOffset.x >= [self st_rightContentOffset].x;
}
- (void)st_scrollToTopAnimated:(BOOL)animated
{
    [self setContentOffset:[self st_topContentOffset] animated:animated];
}
- (void)st_scrollToBottomAnimated:(BOOL)animated
{
    [self setContentOffset:[self st_bottomContentOffset] animated:animated];
}
- (void)st_scrollToLeftAnimated:(BOOL)animated
{
    [self setContentOffset:[self st_leftContentOffset] animated:animated];
}
- (void)st_scrollToRightAnimated:(BOOL)animated
{
    [self setContentOffset:[self st_rightContentOffset] animated:animated];
}
- (NSUInteger)st_verticalPageIndex
{
    return (self.contentOffset.y + (self.frame.size.height * 0.5f)) / self.frame.size.height;
}
- (NSUInteger)st_horizontalPageIndex
{
    return (self.contentOffset.x + (self.frame.size.width * 0.5f)) / self.frame.size.width;
}
- (void)st_scrollToVerticalPageIndex:(NSUInteger)pageIndex animated:(BOOL)animated
{
    [self setContentOffset:CGPointMake(0.0f, self.frame.size.height * pageIndex) animated:animated];
}
- (void)st_scrollToHorizontalPageIndex:(NSUInteger)pageIndex animated:(BOOL)animated
{
    [self setContentOffset:CGPointMake(self.frame.size.width * pageIndex, 0.0f) animated:animated];
}

@end
