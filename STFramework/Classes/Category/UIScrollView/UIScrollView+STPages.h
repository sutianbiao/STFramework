//
//  UIScrollView+STPages.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (STPages)

- (NSInteger)st_pages;
- (NSInteger)st_currentPage;
- (CGFloat)st_scrollPercent;

- (CGFloat)st_pagesY;
- (CGFloat)st_pagesX;
- (CGFloat)st_currentPageY;
- (CGFloat)st_currentPageX;
- (void)st_setPageY:(CGFloat)page;
- (void)st_setPageX:(CGFloat)page;
- (void)st_setPageY:(CGFloat)page animated:(BOOL)animated;
- (void)st_setPageX:(CGFloat)page animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
