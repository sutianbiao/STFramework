//
//  UITableViewCell+STExtension.m
//  STFramework
//
//  Created by jerry on 2019/5/6.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "UITableViewCell+STExtension.h"
#import "UIview+STExtension.h"

@implementation UITableViewCell (STExtension)

/**
 *  创建cell
 *
 *  @param tableView 所属tableView
 *
 *  @return cell实例
 */
+(instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *rid = @"cellID";
    
    //从缓存池中取出cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:rid];
    
    //缓存池中无数据
    if(cell == nil){
        cell = [self viewFromXIB];
    }
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:rid];
    }
    
    return cell;
}

@end
