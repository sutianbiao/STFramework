//
//  UITextField+STBlocks.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (STBlocks)

@property (copy, nonatomic) BOOL (^st_shouldBegindEditingBlock)(UITextField *textField);
@property (copy, nonatomic) BOOL (^st_shouldEndEditingBlock)(UITextField *textField);
@property (copy, nonatomic) void (^st_didBeginEditingBlock)(UITextField *textField);
@property (copy, nonatomic) void (^st_didEndEditingBlock)(UITextField *textField);
@property (copy, nonatomic) BOOL (^st_shouldChangeCharactersInRangeBlock)(UITextField *textField, NSRange range, NSString *replacementString);
@property (copy, nonatomic) BOOL (^st_shouldReturnBlock)(UITextField *textField);
@property (copy, nonatomic) BOOL (^st_shouldClearBlock)(UITextField *textField);

- (void)setSt_shouldBegindEditingBlock:(BOOL (^)(UITextField *textField))shouldBegindEditingBlock;
- (void)setSt_shouldEndEditingBlock:(BOOL (^)(UITextField *textField))shouldEndEditingBlock;
- (void)setSt_didBeginEditingBlock:(void (^)(UITextField *textField))didBeginEditingBlock;
- (void)setSt_didEndEditingBlock:(void (^)(UITextField *textField))didEndEditingBlock;
- (void)setSt_shouldChangeCharactersInRangeBlock:(BOOL (^)(UITextField *textField, NSRange range, NSString *string))shouldChangeCharactersInRangeBlock;
- (void)setSt_shouldClearBlock:(BOOL (^)(UITextField *textField))shouldClearBlock;
- (void)setSt_shouldReturnBlock:(BOOL (^)(UITextField *textField))shouldReturnBlock;

@end

NS_ASSUME_NONNULL_END
