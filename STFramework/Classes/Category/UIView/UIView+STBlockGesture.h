//
//  UIView+STBlockGesture.h
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^STGestureActionBlock)(UIGestureRecognizer *gestureRecoginzer);

@interface UIView (STBlockGesture)

/**
 *  @brief  添加tap手势
 *
 *  @param block 代码块
 */
- (void)st_addTapActionWithBlock:(STGestureActionBlock)block;
/**
 *  @brief  添加长按手势
 *
 *  @param block 代码块
 */
- (void)st_addLongPressActionWithBlock:(STGestureActionBlock)block;

@end

NS_ASSUME_NONNULL_END
