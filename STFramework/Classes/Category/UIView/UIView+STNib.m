//
//  UIView+STNib.m
//  STFramework
//
//  Created by jerry on 2019/5/2.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "UIView+STNib.h"

@implementation UIView (STNib)
#pragma mark - STNibs

+ (UINib *)loadSTNib
{
    return [self loadSTNibNamed:NSStringFromClass([self class])];
}

+ (instancetype)loadSTNibView{
    return  [[[UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil]instantiateWithOwner:nil options:nil] objectAtIndex:0];
}

+ (UINib *)loadSTNibNamed:(NSString*)nibName
{
    return [self loadSTNibNamed:nibName bundle:[NSBundle mainBundle]];
}
+ (UINib *)loadSTNibNamed:(NSString*)nibName bundle:(NSBundle *)bundle
{
    return [UINib nibWithNibName:nibName bundle:bundle];
}
+ (instancetype)loadInstanceFromSTNib
{
    return [self loadInstanceFromSTNibWithName:NSStringFromClass([self class])];
}
+ (instancetype)loadInstanceFromSTNibWithName:(NSString *)nibName
{
    return [self loadInstanceFromSTNibWithName:nibName owner:nil];
}
+ (instancetype)loadInstanceFromSTNibWithName:(NSString *)nibName owner:(id)owner
{
    return [self loadInstanceFromSTNibWithName:nibName owner:owner bundle:[NSBundle mainBundle]];
}
+ (instancetype)loadInstanceFromSTNibWithName:(NSString *)nibName owner:(id)owner bundle:(NSBundle *)bundle
{
    UIView *result = nil;
    NSArray* elements = [bundle loadNibNamed:nibName owner:owner options:nil];
    for (id object in elements)
    {
        if ([object isKindOfClass:[self class]])
        {
            result = object;
            break;
        }
    }
    return result;
}

@end
