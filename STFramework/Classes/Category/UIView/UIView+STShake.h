//
//  UIView+STShake.h
//  Animations
//  抖动
//  Created by YouXianMing on 16/2/25.
//  Copyright © 2016年 YouXianMing. All rights reserved.
//

#import <UIKit/UIKit.h>

// https://github.com/rFlex/SCViewSTShaker

#define kDefaultSTShakeOptions  (SCSTShakeOptionsDirectionHorizontal | SCSTShakeOptionsForceInterpolationExpDown | SCSTShakeOptionsAtEndComplete)
#define kDefaultSTShakeForce    (0.075)
#define kDefaultSTShakeDuration (0.5)
#define kDefaultSTShakeIterationDuration (0.03)

typedef NS_ENUM(NSUInteger,SCSTShakeOptions){
    
    SCSTShakeOptionsDirectionRotate                = 0,
    SCSTShakeOptionsDirectionHorizontal            = 1,
    SCSTShakeOptionsDirectionVertical              = 2,
    SCSTShakeOptionsDirectionHorizontalAndVertical = 3,
    SCSTShakeOptionsForceInterpolationNone         = 4,
    SCSTShakeOptionsForceInterpolationLinearUp     = 8,
    SCSTShakeOptionsForceInterpolationLinearDown   = 16,
    SCSTShakeOptionsForceInterpolationExpUp        = 32,
    SCSTShakeOptionsForceInterpolationExpDown      = 64,
    SCSTShakeOptionsForceInterpolationRandom       = 128,
    SCSTShakeOptionsAtEndRestart                   = 256,
    SCSTShakeOptionsAtEndComplete                  = 512,
    SCSTShakeOptionsAtEndContinue                  = 1024,
    SCSTShakeOptionsAutoreverse                    = 2048
    
} ;

typedef void(^STShakeCompletionHandler)(void);

@interface UIView (STShake)

/**
 *  Returns whether the view is currently shaking or not.
 */
@property (readonly, nonatomic) BOOL isShaking;

/**
 *  STShake the view using the default options. The view will be shaken for a short amount of time.
 */
- (void)shake;

/*
 STShake the view using the specified options.
 |shakeOptions| is an enum of options that can be activated by using the OR operator (like SCSTShakeOptionsDirectionRotate | SCSTShakeOptionsForceInterpolationNone).
 
 |force| is the coefficient of force to apply on each shake iteration (typically between 0 and 1 even though nothing prevents you for setting a higher value if you want).
 
 |duration| is the total duration of the shaking motion. This may be ignored depending of the options you set.
 iterationDuration is how long each shake iteration will last. You may want to set a very low value (like 0.02) if you want a proper shake effect.
 
 |completionHandler|, if not null, is the block that will be invoked when the shake finishes.
 */
- (void)shakeWithOptions:(SCSTShakeOptions)shakeOptions
                   force:(CGFloat)force duration:(CGFloat)duration
       iterationDuration:(CGFloat)iterationDuration
       completionHandler:(STShakeCompletionHandler)completionHandler;

/**
 *  End the current shaking action, if any
 */
- (void)endSTShake;

@end
